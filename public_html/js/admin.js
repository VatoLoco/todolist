$(function () {
    var APPLICATION_ID = "262056DB-85D5-AB16-FF4C-49565464FE00",
            SECRET_KEY = "D100311E-F6F9-CA99-FF96-AB429D04CB00",
            VERSION = "v1";


    var loginScript = $("#login-template").html();
    var loginTemplate = Handlebars.compile(loginScript);

    $('.main-container').html(loginTemplate);

    $(document).on('submit', '.form-signin', function(event) {
        event.preventDefault();

        var data = $(this).serializeArray(),
                email = data[0].value,
                password = data[1].value;
        Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
    });

    function Posts(args) {
        args = args || {};
        this.title = args.title || "";
        this.content = args.content || "";
        this.authorEmail = args.authorEmail || "";
    }

    function userLoggedIn(user) {
        console.log("user successfully logged in");

        var welcomeScript = $('#welcome-template').html();
        var welcomeTemplate = Handlebars.compile(welcomeScript);
        var welcomeHTML = welcomeTemplate(user);

        $('.main-container').html(welcomeHTML);
    }
});

function gotError(error) {
    console.log("Error message - " + error.message);
    console.log("Error code - " + error.code);
}